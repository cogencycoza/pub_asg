# ASG Support Functions Toolbox

## Description
This repository contains Matlab support tools that are useful for Cogency's ASG
software. Specifically, manipulation of STL mesh models for obtaining better
sphere fits.

http://www.cogency.co.za
					
## Usage

1. Check out repository
2. Start Matlab (64bit is required)
3. Change folder to matlab_tools subfolder (or use addpath)
4. Run asg_test1 

## License:
This directory contains a combination of Cogency's library functions
and various dependencies. 

External Dependencies are governed by their own licenses. 
The following dependencies are included:

* Meshfix
* readOFF.m / writeOFF.m / eat_comments.m
* tilefigs

The remaining code is governed by the *Cogency License*:
Cogency CC  hereby grants the user worldwide perpetual license to use, 
modify and/or publish the included items for non-commercial purposes.

Cogency retains copyright and all rights to any intellectual property contained 
in the included items.

## Disclaimer:
ASG is provided AS-IS. Cogency CC is not liable or responsible
for any problems related to data loss, damage or any other
undesirable occurrence caused by the use or transmission of the
provided code.

### Contact:

Email: info@cogency.co.za



