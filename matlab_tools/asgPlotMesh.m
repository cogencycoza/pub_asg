% asgPlotMesh - Display a mesh given vertices and connectivity
%
% Usage: [handle] = asgPlotMesh(pMesh, kMesh, varargin)
%
% handle           = Matlab figure handle
% pMesh   = mesh vertices (3 x p) array
% kMesh   = mesh connectivity (3 x k) array
%
% varargin = zero or more of the following variable parameters:
%   These are the standard Matlab plotting parameters.
%   e.g. 
%       'facecolor','r' % makes mesh faces red
%       'edgecolor','k' % shows triangle edges
%       'facealpha',0.5 % adds transparency
% 
% nb: varargin parameters must be specified as pairs of parameter names and values. 
% 
% Examples:
% >> asgPlotMesh(pMesh, kMesh, 'facecolor','r', 'edgecolor','none')
% 
% See also asgPlotSphere, agsPlotClump.
% 
% ASG Matlab Toolbox
% Copyright Cogency cc 2009
% info@cogency.co.za
% http://www.cogency.co.za
%
function [handle] = asgPlotMesh(vertices, faces, varargin)

if isempty(varargin)

    asgFig3d('light', true);
    ph = patch('vertices', vertices',... 
           'faces', faces',...
           'facecolor', 'r',...
           'edgecolor', 'none');
    axis equal       
else
    ph = patch('vertices', vertices',... 
               'faces', faces',...
                varargin{:});
    axis equal
end

if nargout==1
    handle = ph;
end
               
               