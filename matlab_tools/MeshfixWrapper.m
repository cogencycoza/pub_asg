function [pMesh2, kMesh2] = MeshFixWrapper(pMesh, kMesh) 

  if all(CheckMeshManifoldMex(pMesh, kMesh)==0)
      % all fine
      pMesh2 = pMesh;
      kMesh2 = kMesh;
      return;
  end
  
  % Call Marco Atene's MESHFIX on a mesh
  %
  prefix = tempname;
  off_filename = [prefix '.off'];
  off_filename_fixed = [prefix '_fixed.off'];
  writeOFF(off_filename,pMesh',kMesh');
  flags = '';
  path_to_meshfix = which('meshfix.exe');
  command = [path_to_meshfix ' ' flags ' ' off_filename];
  [status, result] = system(command);
  if status ~= 0
    fprintf(command);
    error(result);
  end
  [pMesh2, kMesh2] = readOFF(off_filename_fixed);
  pMesh2 = pMesh2';
  kMesh2 = kMesh2';

  delete(off_filename);
  delete(off_filename_fixed);
end
