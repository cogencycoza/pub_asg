% asgWriteStl - Write mesh to STL binary file
% 
% Usage: asgWriteStl(pMesh, kMesh, filename)
%
% pMesh   = mesh vertices (3 x p) array
% kMesh   = mesh connectivity (3 x k) array
% filename = filename of STL file, e.g. 'c:\data\mystl.stl'
%
% Examples:
%     asgWriteStl(pMesh, kMesh, './output/test.stl')
%
% See also asgReadStl.
% 
% ASG Matlab Toolbox
% Copyright Cogency cc 2009
% info@cogency.co.za
% http://www.cogency.co.za
%
% References:
% 1) http://en.wikipedia.org/wiki/STL_(file_format)
%
function asgWriteStl(pMesh, kMesh, filename, varargin)

forceOverwrite = false;
cVararginModify(varargin{:})
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (exist(filename) == 2) 
    if ~forceOverwrite
        disp('WARNING: file already exists! Are you sure you want to overwrite?');
        disp('WARNING: Press any key to continue, or CTRL-C to quit!');    
        pause
    end
    delete(filename);
end

[aa bb ext] = fileparts(filename);
if (~strcmpi(ext, '.stl'))
    error('Specified output file does not have .stl extension. Please amend the filename.');
end
       
AsgWriteStlMex(pMesh, kMesh, filename);

