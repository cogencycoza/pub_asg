function cVararginModify( varargin )

if (  mod(length( varargin),2) == 1  ), error('Need even number of arguments: variable name followed by value'); end
for loop = 1:2:length( varargin)
    
    if evalin('caller', ['exist(''',varargin{loop},''')'])                
         assignin('caller', varargin{loop}, varargin{loop+1} );
         
    else
        error( sprintf('Unknown variable %s (specified value: %g)', varargin{loop}, varargin{loop+1} ) )
    end
end
