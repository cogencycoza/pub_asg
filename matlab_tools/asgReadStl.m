% asgReadStl - Read STL triangluar mesh files into Matlab
% 
% Usage: [pMesh kMesh] = asgReadStl(filename, varargin)
%
% pMesh   = mesh vertices (3 x p) array
% kMesh   = mesh connectivity (3 x k) array
% filename = filename of STL file, e.g. 'c:\data\mystl.stl'
% varargin = zero or more of the following variable parameters:
%   'draw' = true/false (plot the mesh after loading. default=false)
%
% nb: varargin parameters must be specified as pairs of parameter names and values. 
%
% Examples:
%     [p k] = asgReadStl('xyz.stl', 'draw', true) % read xyz.stl and draw the result 
%     asgReadStl('xyz.stl') % read xyz.stl and only draw.  
% 
% See also asgWriteStl.
%
% ASG Matlab Toolbox
% Copyright Cogency cc 2009
% info@cogency.co.za
% http://www.cogency.co.za
%
% References:
% 1) http://en.wikipedia.org/wiki/STL_(file_format)
%
function [pMesh, kMesh] = asgReadStl(filename, varargin)

draw = false;
cVararginModify(varargin{:})
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (exist(filename) ~= 2) 
    error('Specified file does not exist.');
end

[aa bb ext] = fileparts(filename);
if (~strcmpi(ext, '.stl'))
    error('Specified file does not have .stl extension. If it is NOT an STL file, it may cause the reader to crash. If this is an stl file, then please rename to correct extension.');
end
       
[p k] = AsgReadStlMex(filename);

if draw || nargout==0
    h = asgFig3d('light', true);
    patch('vertices', p', 'faces', k', 'facecolor', 'g')
    axis equal
    set(h,'name', filename)
end

if nargout>0
    pMesh = p;
    kMesh = k;
end


