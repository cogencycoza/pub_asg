% asgFig3d - Open a new Matlab figure window and prepare for 3D viewing
% 
% Usage: [figure_handle] = asgFig3d(varargin)
%
% figure_handle = the returned handle from Matlab. 
% name         = a name for your particle (used to identify particle in XML file)
% materialName = name of corresponding material , SEE asgMaterial for valid options
% pMesh        = mesh vertices (3 x p) array
% kMesh        = mesh connectivity (3 x k) array
% spheres      = (n x 4) array of sphere parameters. i.e. each row is x,y,z,r
%
% varargin = zero or more of the following variable parameters:
%   'renderer' = 'opengl' (windows) / 'zbuffer' linux - Override renderer
%   'light' = false (enable/disable camera light
%   'visible' = on  (set visible flag of window)
%
% nb: varargin parameters must be specified as pairs of parameter names and values. 
%
% Examples:
%     asgFig3d
%     asgFig3d('light', true) % enable lights
%     asgFig3d('renderer', ,'opengl') % force opengl renderer
% 
% ASG Matlab Toolbox
% Copyright Cogency cc 2009
% info@cogency.co.za
% http://www.cogency.co.za
%
%
function [fh] = asgFig3d ( varargin )

if isunix
    renderer = 'zbuffer'; % some linux systems have problems with opengl driver
else
    renderer = 'opengl';
end

light = false; %this makes the rendering faster -- use only the relevant portion of the image
visible = 'on';

cVararginModify( varargin{:} );

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (  datenum(version('-date')) < datenum('23 Sep 2000') )
    warningState = warning;

    if ( ~strcmp(warningState, 'off') )
        disp('Turning warning off to prevent irritating unrecognized OpenGL version warnings')
        warning off;
    end
end

handle = figure('Visible', visible);
drawnow 

hold on
cameratoolbar
axis off
axis equal
axis vis3d
view(3)
cameratoolbar('ResetCameraAndSceneLight')
axis equal
set(handle, 'renderer', renderer)

if light > 0
    cameratoolbar togglescenelight
end

if nargout > 0
    fh = handle;
end

