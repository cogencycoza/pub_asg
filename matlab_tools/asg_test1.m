% Demonstration code to downsample STL models
%
clear all
close all
clc

echo on

% select input file
stlFile = fullfile('..', 'sample_data', '28_b1.stl');

% read STL
[ps, ks] = asgReadStl(stlFile);

% plot original STL
asgFig3d
asgPlotMesh(ps, ks, 'facecolor', 'r')
title(sprintf('Original with %d vertices', size(ps,2)), 'fontsize', 14)
drawnow

% Fix connectivity of original mesh
[ps2, ks2] = MeshfixWrapper(ps, ks);

% Test mesh connectivity
chk = CheckMeshManifoldMex(ps2, ks2);
disp(chk) % should be all zeros for a good watertight mesh

% Decimate mesh to N vertices
for N=[5000, 1000, 500]
    [pd kd] = DecimateMeshMex(ps2, ks2, N);

    % Recheck and fix manifold after decimation
    [pd, kd] = MeshfixWrapper(pd, kd);
    chk = CheckMeshManifoldMex(pd, kd);
    disp(chk)

    % plot final mesh
    asgFig3d
    asgPlotMesh(pd, kd, 'facecolor','g')
    title(sprintf('Decimated to %d vertices', N), 'fontsize', 14)
    drawnow

    tilefigs % tile the figures

    % Save output mesh to new stl file
    [pth nme ext] = fileparts(stlFile);
    outfile = fullfile(pth, [nme '_decimate_' num2str(N) ext]);
    delete(outfile)
    asgWriteStl(pd, kd, outfile);
end

echo off